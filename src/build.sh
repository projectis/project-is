#!/bin/bash
MCVERSION="1.5.2"
MAJOR="0"
MINOR="1"

if [ -d "builds" ]; then
  rm -r builds
fi
mkdir builds
if [ -d "$JENKINS_HOME/forge/mcp/src/minecraft/projectis" ]; then
  rm -r $JENKINS_HOME/forge/mcp/src/minecraft/projectis
fi
if [ -d "$JENKINS_HOME/forge/mcp/src/minecraft/screenshotuploader" ]; then
  rm -r $JENKINS_HOME/forge/mcp/src/minecraft/screenshotuploader
fi
if [ -d "$JENKINS_HOME/forge/mcp/src/minecraft/playerbeacons" ]; then
  rm -r $JENKINS_HOME/forge/mcp/src/minecraft/playerbeacons
fi
mkdir $JENKINS_HOME/forge/mcp/src/minecraft/projectis
cp -r $WORKSPACE/* $JENKINS_HOME/forge/mcp/src/minecraft/projectis
cd $JENKINS_HOME/forge/mcp/
sh recompile.sh
sh reobfuscate_srg.sh
cd $JENKINS_HOME/forge/mcp/reobf/minecraft
zip -r -D -9 $WORKSPACE/builds/ProjectIS_$MCVERSION-$MAJOR.$MINOR.$BUILD_NUMBER.zip *
rm -r $JENKINS_HOME/forge/mcp/src/minecraft/projectis