package projectis.tileentity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;

public class TileEntityAlloySmelter extends TileEntity implements ISidedInventory{
	
	private ItemStack[] alloyItemStacks = new ItemStack[3];

	@Override
	public int getSizeInventory() {
		return this.alloyItemStacks.length;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return this.alloyItemStacks[i];
	}

	@Override
	public ItemStack decrStackSize(int i, int j) {
        if (this.alloyItemStacks[i] != null) {
            ItemStack itemstack;

            if (this.alloyItemStacks[i].stackSize <= j) {
                itemstack = this.alloyItemStacks[i];
                this.alloyItemStacks[i] = null;
                return itemstack;
            }
            else {
                itemstack = this.alloyItemStacks[i].splitStack(j);

                if (this.alloyItemStacks[i].stackSize == 0) {
                    this.alloyItemStacks[i] = null;
                }

                return itemstack;
            }
        }
        else {
            return null;
        }
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
        if (this.alloyItemStacks[i] != null) {
            ItemStack itemstack = this.alloyItemStacks[i];
            this.alloyItemStacks[i] = null;
            return itemstack;
        }
        else {
            return null;
        }
	}

	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack) {
		this.alloyItemStacks[i] = itemstack;
		if (itemstack != null && itemstack.stackSize > this.getInventoryStackLimit()) {
			itemstack.stackSize = this.getInventoryStackLimit();
		}
	}

	@Override
	public String getInvName() {
		return "container.alloyfurnace";
	}

	@Override
	public boolean isInvNameLocalized() {
		return false;
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
        return this.worldObj.getBlockTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : entityplayer.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
    }

	@Override
	public void openChest() { }

	@Override
	public void closeChest() { }

	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) {
		return false;  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int side) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean canInsertItem(int i, ItemStack itemstack, int j) {
		return false;  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public boolean canExtractItem(int slot, ItemStack itemstack, int side) {
		return side != 0 || slot != 1;
	}
}
