package projectis.tileentity;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityLiving;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class TileEntityAssemblyProcessingUnit extends TileEntity {
	
	private boolean structureStatus;
	private int xOffset;
	private int zOffset;
	private int facing;

	public void updateEntity() {
		if (!this.worldObj.isRemote && (this.worldObj.getWorldTime() % 20) == 0) {
			if (structureStatus == false) {
				if (checkStructure()) structureStatus = true;
				else structureStatus = false;
			}
		}
	}

    public void readFromNBT(NBTTagCompound NBTTagCompound) {
        super.readFromNBT(NBTTagCompound);
        structureStatus = NBTTagCompound.getBoolean("structureStatus");
    }

    public void writeToNBT(NBTTagCompound NBTTagCompound) {
        super.writeToNBT(NBTTagCompound);
        NBTTagCompound.setBoolean("structureStatus", structureStatus);
    }
	
	public void intialSetup(World world, int side) {
        int l = side;

        //Player facing south
        //X increases as player heads left, z increases as player moves forward
        if (l == 0) {
        	this.facing = 0;
        	System.out.println("Facing 0");
        }
        //Player facing west
        //z increases as player heads left, x decreases as player heads forward
        if (l == 1) {
        	this.facing = 1;
        	System.out.println("Facing 1");
        }
        //Player facing north
        //x decreases as player heads left, z decreases as player heads forward
        if (l == 2) {
        	this.facing = 2;
        	System.out.println("Facing 2");
        }
        //Player facing east
        //z decreases as player heads left, x increases as player heads forward
        if (l == 3) {
        	this.facing = 3;
        	System.out.println("Facing 3");
        }
        
        if (this.checkStructure()) this.structureStatus = true;
		else this.structureStatus = false;
    }
	
	/*
	 * TODO
	 * Once it is verified as a valid structure, we change all iron blocks into a custom blank block.
	 * The blank block will have the location of the CPU and will notify the CPU once it's broken 
	 * and the CPU will tell all the blocks to revert back to their normal state (ie iron)
	 * 
	 * Blank blocks don't need to receive tick do set canUpdate to false on them
	 */
	private boolean checkStructure() {
		
		long start = System.currentTimeMillis();
		
		//TODO I'm 90% sure this code is terrible and needs to be re-written

		if (this.facing == 0) {
			for (int i = -1; i < 6; i++) {
				//left/right
				for (int j = -2; j < 3; j++) {
					// front/back
					for (int k = 0; k < 4; k++) {
						if ((i == 0) && (j == 0) && (k == 0)) { }
						else if ((i == 2) && (k == 2) && ((j == -2) || (j == 2)) && (this.worldObj.getBlockTileEntity(this.xCoord + j, this.yCoord + i, this.zCoord + k) instanceof TileEntityAssembler)) { }
						else if (((i == 2) && (j == 0) && (k == 3)) && (!(this.worldObj.getBlockTileEntity(this.xCoord + j, this.yCoord + i, this.zCoord + k) instanceof TileEntityAssemblyPowerInlet))) return false;
						else if ((i == 0) && ((j != -2) || (j != 2)) && ((k != 0) && (k != 3))) { }
						else if (((i == 1) || (i == 3)) && ((j != -2) || (j != 2)) && ((k == 1) || (k == 2))) { }
						else if (((i == 1) || (i == 3)) && (k == 0)) { }
						else if ((i == 2) && (k == 0)) { }
						else if ((i == 2) && ((j != -2) || (j != 2))) { }
						else if ((i == 4) && (k != 3)) { }
						else if ((i == 5) && (k == 3) && ((j == -2) || (j == 2))) { }
						else if ((i == 5) && (k != 3)) { }
						else if (this.worldObj.getBlockId(this.xCoord + j, this.yCoord + i, this.zCoord + k) != Block.blockIron.blockID) return false;
					}
				}
			}
		}
		else if (this.facing == 2) {
			for (int i = -1; i < 6; i++) {
				//left/right
				for (int j = -2; j < 3; j++) {
					// front/back
					for (int k = 0; k < 4; k++) {
						if ((i == 0) && (j == 0) && (k == 0)) { }
						else if ((i == 2) && (k == 2) && ((j == -2) || (j == 2)) && (this.worldObj.getBlockTileEntity(this.xCoord + j, this.yCoord + i, this.zCoord - k) instanceof TileEntityAssembler)) { }
						else if (((i == 2) && (j == 0) && (k == 3)) && (!(this.worldObj.getBlockTileEntity(this.xCoord + j, this.yCoord + i, this.zCoord - k) instanceof TileEntityAssemblyPowerInlet))) return false;
						else if ((i == 0) && ((j != -2) || (j != 2)) && ((k != 0) && (k != 3))) { }
						else if (((i == 1) || (i == 3)) && ((j != -2) || (j != 2)) && ((k == 1) || (k == 2))) { }
						else if (((i == 1) || (i == 3)) && (k == 0)) { }
						else if ((i == 2) && (k == 0)) { }
						else if ((i == 2) && ((j != -2) || (j != 2))) { }
						else if ((i == 4) && (k != 3)) { }
						else if ((i == 5) && (k == 3) && ((j == -2) || (j == 2))) { }
						else if ((i == 5) && (k != 3)) { }
						else if (this.worldObj.getBlockId(this.xCoord + j, this.yCoord + i, this.zCoord - k) != Block.blockIron.blockID) return false;
					}
				}
			}
		}
		else if (this.facing == 1) {
			for (int i = -1; i < 6; i++) {
				//left/right
				for (int j = -2; j < 3; j++) {
					// front/back
					for (int k = 0; k < 4; k++) {
						if ((i == 0) && (j == 0) && (k == 0)) { }
						else if ((i == 2) && (k == 2) && ((j == -2) || (j == 2)) && (this.worldObj.getBlockTileEntity(this.xCoord - k, this.yCoord + i, this.zCoord + j) instanceof TileEntityAssembler)) { }
						else if (((i == 2) && (j == 0) && (k == 3)) && (!(this.worldObj.getBlockTileEntity(this.xCoord - k, this.yCoord + i, this.zCoord + j) instanceof TileEntityAssemblyPowerInlet))) return false;
						else if ((i == 0) && ((j != -2) || (j != 2)) && ((k != 0) && (k != 3))) { }
						else if (((i == 1) || (i == 3)) && ((j != -2) || (j != 2)) && ((k == 1) || (k == 2))) { }
						else if (((i == 1) || (i == 3)) && (k == 0)) { }
						else if ((i == 2) && (k == 0)) { }
						else if ((i == 2) && ((j != -2) || (j != 2))) { }
						else if ((i == 4) && (k != 3)) { }
						else if ((i == 5) && (k == 3) && ((j == -2) || (j == 2))) { }
						else if ((i == 5) && (k != 3)) { }
						else if (this.worldObj.getBlockId(this.xCoord - k, this.yCoord + i, this.zCoord + j) != Block.blockIron.blockID) return false;
					}
				}
			}
		}
		else if (this.facing == 3) {
			for (int i = -1; i < 6; i++) {
				//left/right
				for (int j = -2; j < 3; j++) {
					// front/back
					for (int k = 0; k < 4; k++) {
						this.worldObj.setBlock(this.xCoord + k, this.yCoord + i, this.zCoord + j, 7);
						if ((i == 0) && (j == 0) && (k == 0)) { }
						else if ((i == 2) && (k == 2) && ((j == -2) || (j == 2)) && (this.worldObj.getBlockTileEntity(this.xCoord + k, this.yCoord + i, this.zCoord + j) instanceof TileEntityAssembler)) { }
						else if (((i == 2) && (j == 0) && (k == 3)) && (!(this.worldObj.getBlockTileEntity(this.xCoord + k, this.yCoord + i, this.zCoord + j) instanceof TileEntityAssemblyPowerInlet))) return false;
						else if ((i == 0) && ((j != -2) || (j != 2)) && ((k != 0) && (k != 3))) { }
						else if (((i == 1) || (i == 3)) && ((j != -2) || (j != 2)) && ((k == 1) || (k == 2))) { }
						else if (((i == 1) || (i == 3)) && (k == 0)) { }
						else if ((i == 2) && (k == 0)) { }
						else if ((i == 2) && ((j != -2) || (j != 2))) { }
						else if ((i == 4) && (k != 3)) { }
						else if ((i == 5) && (k == 3) && ((j == -2) || (j == 2))) { }
						else if ((i == 5) && (k != 3)) { }
						else if (this.worldObj.getBlockId(this.xCoord + k, this.yCoord + i, this.zCoord + j) != Block.blockIron.blockID) return false;
					}
				}
			}
		}
		
		System.out.println("It took " + (System.currentTimeMillis() - start) + " milliseconds to check for a machine");
		return true;
	}
	
	/*
	 * Create the Assembler structure
	 * 
	 * Return true for successful, false for not
	 */
	private boolean createStructure() {
		return false;
	}
	
	/*
	 * "Destroy" the structure
	 * 
	 * Return true for successful, false for not
	 */
	public boolean destoryStructure() {
		return false;
	}
}
