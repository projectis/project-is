package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemMiscAntimatterHyperdrive extends Item{

	public ItemMiscAntimatterHyperdrive(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("miscAntimatterHyperdrive");
	}
	
}
