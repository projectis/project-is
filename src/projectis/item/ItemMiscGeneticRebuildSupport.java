package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemMiscGeneticRebuildSupport extends Item{

	public ItemMiscGeneticRebuildSupport(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("miscGeneticRebuildSupport");
	}
	
}
