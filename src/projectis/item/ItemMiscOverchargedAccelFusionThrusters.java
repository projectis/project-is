package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemMiscOverchargedAccelFusionThrusters extends Item{

	public ItemMiscOverchargedAccelFusionThrusters(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("miscOverchargedAccelFusionThrusters");
	}
	
}
