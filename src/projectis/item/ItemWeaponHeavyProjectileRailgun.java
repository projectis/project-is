package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponHeavyProjectileRailgun extends Item{

	public ItemWeaponHeavyProjectileRailgun(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponHeavyProjectileRailgun");
	}
	
}
//shoulder mount