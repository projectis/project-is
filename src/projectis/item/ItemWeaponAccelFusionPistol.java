package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponAccelFusionPistol extends Item{

	public ItemWeaponAccelFusionPistol(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponAccelFusionPistol");
	}
	
}
