package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemUtilTungstenDrill extends Item{

	public ItemUtilTungstenDrill(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("utilTungstenDrill");
	}
	
}
