package projectis.item;

import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemOre extends ItemBlock {
	
	private final static String[] subNames = {"aluminum", "palladium", "silver", "tungsten", "selenium"};

	public ItemOre(int id) {
		super(id);
		this.setHasSubtypes(true);
	}
	
	public int getPlacedBlockMetadata(int i) {
	    return i;
	}
	
	public int getMetadata(int damageValue) {
		return damageValue;
	}
	
	public String getUnlocalizedName(ItemStack itemstack) {
		return subNames[itemstack.getItemDamage()];
	}

}
