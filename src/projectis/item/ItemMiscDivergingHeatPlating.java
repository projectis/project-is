package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemMiscDivergingHeatPlating extends Item{

	public ItemMiscDivergingHeatPlating(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("miscDivergingHeatPlating");
	}
	
}
