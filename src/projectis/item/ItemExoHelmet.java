package projectis.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class ItemExoHelmet extends ItemArmor {
	
	EntityPlayer thePlayer;

	public ItemExoHelmet(int par1, EnumArmorMaterial par2EnumArmorMaterial, int par3, int par4) {
		super(par1, EnumArmorMaterial.DIAMOND, par3, par4);
		this.setNoRepair();
		this.setMaxStackSize(1);
		this.setCreativeTab(CreativeTabs.tabCombat);
		this.setUnlocalizedName("itemExoHelmet");
	}
	
	@Override
	public boolean isBookEnchantable(ItemStack itemstack1, ItemStack itemstack2) {
        return false;
    }

	@SideOnly(Side.CLIENT)
    public void renderHelmetOverlay(ItemStack stack, EntityPlayer player, ScaledResolution resolution, float partialTicks, boolean hasScreen, int mouseX, int mouseY) {

        /*
		Minecraft.getMinecraft().fontRenderer.drawString("Reactor:", 1, 20, 0xffffff, false);
		//Minecraft.getMinecraft().fontRenderer.drawString("Online", (resolution.getScaledWidth() - Minecraft.getMinecraft().fontRenderer.getStringWidth("Online")) / 2, 30, 0xff00, false);
		Minecraft.getMinecraft().fontRenderer.drawString("Online", 1, 30, 0xff00, false);
		Minecraft.getMinecraft().fontRenderer.drawString("Sensors:", 1, 45, 0xffffff, false);
		Minecraft.getMinecraft().fontRenderer.drawString("Online", 1, 55, 0xff00, false);
		Minecraft.getMinecraft().fontRenderer.drawString("Weapons:", 1, 70, 0xffffff, false);
		Minecraft.getMinecraft().fontRenderer.drawString("Online", 1, 80, 0xff00, false);
		*/
		
	}
}
