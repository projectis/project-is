package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemProtectionAdvancedShield extends Item{

	public ItemProtectionAdvancedShield(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("protectionAdvancedShield");
	}
	
}
