package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemCPUAdvanced extends Item{

	public ItemCPUAdvanced(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("cpuAdvanced");
	}
	
}
//Tier 2