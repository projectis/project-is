package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.ItemArmor;

public class ItemExoLegs extends ItemArmor {
	
	public ItemExoLegs(int par1, EnumArmorMaterial par2EnumArmorMaterial, int par3, int par4) {
		super(par1, EnumArmorMaterial.DIAMOND, par3, par4);
		this.setNoRepair();
		this.setMaxStackSize(1);
		this.setCreativeTab(CreativeTabs.tabCombat);
		this.setUnlocalizedName("itemExoLegs");
	}

}
