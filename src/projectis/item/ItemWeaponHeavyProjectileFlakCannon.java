package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponHeavyProjectileFlakCannon extends Item{

	public ItemWeaponHeavyProjectileFlakCannon(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponHeavyProjectileFlakCannon");
	}
	
}
//shoulder mount