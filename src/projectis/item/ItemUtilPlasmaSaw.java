package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemUtilPlasmaSaw extends Item{

	public ItemUtilPlasmaSaw(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("utilPlasmaSaw");
	}
	
}
//shouldermount