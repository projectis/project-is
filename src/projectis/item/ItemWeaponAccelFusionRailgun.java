package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponAccelFusionRailgun extends Item{

	public ItemWeaponAccelFusionRailgun(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponAccelFusionRailgun");
	}
	
}
//shouldermount