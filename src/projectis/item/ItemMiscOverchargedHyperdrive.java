package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemMiscOverchargedHyperdrive extends Item{

	public ItemMiscOverchargedHyperdrive(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("miscOverchargedHyperdrive");
	}
	
}
