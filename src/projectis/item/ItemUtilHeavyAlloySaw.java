package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemUtilHeavyAlloySaw extends Item{

	public ItemUtilHeavyAlloySaw(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("utilHeavyAlloySaw");
	}
	
}
