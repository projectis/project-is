package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponHeavyProjectileSMG extends Item{

	public ItemWeaponHeavyProjectileSMG(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponHeavyProjectileSMG");
	}
	
}
