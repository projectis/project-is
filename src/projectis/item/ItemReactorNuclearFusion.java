package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemReactorNuclearFusion extends Item{

	public ItemReactorNuclearFusion(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("reactorNuclearFusion");
	}
	
}
