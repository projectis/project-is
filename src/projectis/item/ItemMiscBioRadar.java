package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemMiscBioRadar extends Item{

	public ItemMiscBioRadar(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("miscBioRadar");
	}
	
}
