package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemMiscAdditionalThrusters extends Item{

	public ItemMiscAdditionalThrusters(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("miscAdditionalThrustsers");
	}
	
}
