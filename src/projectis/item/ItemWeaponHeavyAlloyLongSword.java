package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponHeavyAlloyLongSword extends Item{

	public ItemWeaponHeavyAlloyLongSword(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponHeavyAlloyLongSword");
	}
	
}
