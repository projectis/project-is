package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponAccelFusionShoulderGun extends Item{

	public ItemWeaponAccelFusionShoulderGun(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponAccelFusionShoulderGun");
	}
	
}
//shoulder mount