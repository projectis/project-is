package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponLaserShoulderGun extends Item{

	public ItemWeaponLaserShoulderGun(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponLaserShoulderGun");
	}
	
}
//Shoulder Mount