package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemUtilHeavyAlloyDrill extends Item{

	public ItemUtilHeavyAlloyDrill(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("utilHeavyAlloyDrill");
	}
	
}
