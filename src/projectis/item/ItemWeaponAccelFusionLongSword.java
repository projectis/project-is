package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponAccelFusionLongSword extends Item{

	public ItemWeaponAccelFusionLongSword(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponAccelFusionLongSword");
	}
	
}
