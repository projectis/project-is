package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import projectis.common.DamageEnergy;

public class ItemWeaponLaserRapier extends Item {

	private final int weaponDamage;

	public ItemWeaponLaserRapier(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponLaserRapier");
		weaponDamage = 10;
	}

	public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity) {
		entity.attackEntityFrom(new DamageEnergy("energy", player), 10);
		return true;
	}

	public int getDamageVsEntity(Entity par1Entity) {
		return 0;
	}
}
