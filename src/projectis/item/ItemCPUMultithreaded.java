package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemCPUMultithreaded extends Item{

	public ItemCPUMultithreaded(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("cpuMultithreaded");
	}
	
}
//Tier 4