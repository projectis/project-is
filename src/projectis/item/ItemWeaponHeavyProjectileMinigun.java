package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponHeavyProjectileMinigun extends Item{

	public ItemWeaponHeavyProjectileMinigun(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponHeavyProjectileMinigun");
	}
	
}
