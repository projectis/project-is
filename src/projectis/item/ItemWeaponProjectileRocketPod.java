package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponProjectileRocketPod extends Item{

	public ItemWeaponProjectileRocketPod(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponProjectileRocketPod");
	}
	
}
//Shoulder Mount