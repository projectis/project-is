package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemMiscZonedLiquidCooling extends Item{

	public ItemMiscZonedLiquidCooling(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("miscZonedLiquidCooling");
	}
	
}
