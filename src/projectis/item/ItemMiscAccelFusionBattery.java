package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemMiscAccelFusionBattery extends Item{

	public ItemMiscAccelFusionBattery(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("miscAccelFusionBattery");
	}
	
}
