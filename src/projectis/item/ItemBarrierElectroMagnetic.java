package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemBarrierElectroMagnetic extends Item{

	public ItemBarrierElectroMagnetic(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("barrierElectroMagnetic");
	}
	
}
