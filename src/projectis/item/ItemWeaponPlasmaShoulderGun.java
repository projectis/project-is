package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponPlasmaShoulderGun extends Item{

	public ItemWeaponPlasmaShoulderGun(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponPlasmaShoulderGun");
	}
	
}
//shoulder mount