package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponAccelFusionSMG extends Item{

	public ItemWeaponAccelFusionSMG(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponAccelFusionSMG");
	}
	
}
