package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponHeavyAlloyShortSword extends Item{

	public ItemWeaponHeavyAlloyShortSword(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponHeavyAlloyShortSword");
	}
	
}
