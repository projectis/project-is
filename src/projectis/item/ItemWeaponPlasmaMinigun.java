package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponPlasmaMinigun extends Item{

	public ItemWeaponPlasmaMinigun(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponPlasmaMinigun");
	}
	
}
