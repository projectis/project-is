package projectis.item;

import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemMachines extends ItemBlock{
	
	private final static String[] subNames = {"Engineering WorkTable", "Research Table", "Processing Plant", "Plate-Pressing Machine", "Alloy Smelter","Assembly Processing Unit","Assembly Power Inlet","Assembler","Assembly Storage"};

	public ItemMachines(int id) {
		super(id);
		this.setHasSubtypes(true);
	}
	
	public int getPlacedBlockMetadata(int i) {
	    return i;
	}
	
	public int getMetadata(int damageValue) {
		return damageValue;
	}
	
	public String getUnlocalizedName(ItemStack itemstack) {
		return subNames[itemstack.getItemDamage()];
	}


}
