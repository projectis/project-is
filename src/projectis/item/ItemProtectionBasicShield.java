package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemProtectionBasicShield extends Item{

	public ItemProtectionBasicShield(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("proctecionBasicShield");
	}
	
}
