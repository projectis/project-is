package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemProtectionHeavyAlloyShield extends Item{

	public ItemProtectionHeavyAlloyShield(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("protectionHeavyAlloyShield");
	}
	
}
