package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponPlasmaBardiche extends Item{

	public ItemWeaponPlasmaBardiche(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponPlasmaBardiche");
	}
	
}
