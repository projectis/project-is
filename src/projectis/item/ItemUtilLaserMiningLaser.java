package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemUtilLaserMiningLaser extends Item{

	public ItemUtilLaserMiningLaser(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("utilLaserMiningLaser");
	}
	
}
//shoulderMount