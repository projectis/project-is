package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemMiscHyperdrivePrototype extends Item{

	public ItemMiscHyperdrivePrototype(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("miscHyperdrivePrototype");
	}
	
}
