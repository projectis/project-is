package projectis.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemWeaponProjectileMinigun extends Item{

	public ItemWeaponProjectileMinigun(int id){
		super(id);
		maxStackSize = 1;
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("weaponProjectileMinigun");
	}
	
}
//shoulder mount