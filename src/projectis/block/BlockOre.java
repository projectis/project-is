package projectis.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

import java.util.List;

public class BlockOre extends Block {
	
    public static final String[] oreType = new String[] {"Aluminum", "Palladium", "Silver", "Tungsten", "Selenium"};
    public static final String[] oreTextures = new String[] {"ore_aluminum", "ore_palladium", "ore_silver", "ore_tungsten", "ore_selenium"};
	@SideOnly(Side.CLIENT)
	private Icon[] iconArray;

	public BlockOre(int par1, Material par2Material) {
		super(par1, Material.ground);
		this.setCreativeTab(CreativeTabs.tabBlock);
		this.setUnlocalizedName("ore");
	}
	
    public Icon getIcon(int side, int meta) {
        if (meta < 0 || meta >= this.iconArray.length) {
            meta = 0;
        }
        return this.iconArray[meta];
    }

    public int damageDropped(int par1) {
        return par1;
    }

    @SideOnly(Side.CLIENT)
    public void getSubBlocks(int id, CreativeTabs creativetab, List list) {
        list.add(new ItemStack(id, 1, 0));
        list.add(new ItemStack(id, 1, 1));
        list.add(new ItemStack(id, 1, 2));
        list.add(new ItemStack(id, 1, 3));
        list.add(new ItemStack(id, 1, 4));
    }
    
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister) {
        this.iconArray = new Icon[oreTextures.length];

        for (int i = 0; i < this.iconArray.length; ++i)
        {
            this.iconArray[i] = par1IconRegister.registerIcon(oreTextures[i]);
        }
    }

}
