package projectis.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import projectis.ProjectIS;
import projectis.tileentity.*;

import java.util.List;

public class BlockMachines extends Block {
	public static final String[] machineType = new String[] {"Engineering WorkTable", "Research Table", "Processing Plant", "Plate-Pressing Machine", "Alloy Smelter","Assembly Processing Unit","Assembly Power Inlet","Assembler","Assembly Storage"};
    public static final String[] machineTexture = new String[] {"mcn_EnWorkTable", "mcn_ResearchTable", "mcn_ProcessingPlant", "mcn_PlatePress", "mcn_AlloySmelter","mcn_AssemblyCPU","mcn_AssemblyPwr","mcn_Assembler","mcn_AssemblyStorage"};
	
    @SideOnly(Side.CLIENT)
	private Icon[] iconArray;
	
	public BlockMachines(int par1, Material par2Material) {
		super(par1, Material.iron);
		this.setCreativeTab(CreativeTabs.tabBlock);
		this.setUnlocalizedName("machine");
	}
	
	public Icon getIcon(int side, int meta) {
        if (meta < 0 || meta >= this.iconArray.length) {
            meta = 0;
        }
        return this.iconArray[meta];
    }

    public int damageDropped(int par1) {
        return par1;
    }
    
	@Override
    public boolean hasTileEntity(int metadata) {
    	return true;
    }
    
    @Override
    public TileEntity createTileEntity(World world, int metadata) {
    	switch (metadata) {
    	case 0: return new TileEntityEngineeringWorktable();
    	case 1: return new TileEntityResearchTable();
    	case 2: return new TileEntityProcessingPlant();
    	case 3: return new TileEntityPlatePressing();
    	case 4: return new TileEntityAlloySmelter();
    	case 5: return new TileEntityAssemblyProcessingUnit();
    	case 6: return new TileEntityAssemblyPowerInlet();
    	case 7: return new TileEntityAssembler();
    	case 8: return new TileEntityAssemblyStorage();
    	default: return new TileEntityEngineeringWorktable();
    	}
	}
    
    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {
        player.openGui(ProjectIS.instance, 0, world, x, y, z);
        return true;
    }
    
    @Override
    public int onBlockPlaced(World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int meta) {
    	if (!world.isRemote) {
    		switch (meta) {
    		case 5: {
    			TileEntityAssemblyProcessingUnit tileEntity = (TileEntityAssemblyProcessingUnit) world.getBlockTileEntity(x, y, z);
        		tileEntity.intialSetup(world, side);
    		}
    		}
    	}
		return meta;
    }

    @SideOnly(Side.CLIENT)
    public void getSubBlocks(int id, CreativeTabs creativetab, List list) {
        list.add(new ItemStack(id, 1, 0));
        list.add(new ItemStack(id, 1, 1));
        list.add(new ItemStack(id, 1, 2));
        list.add(new ItemStack(id, 1, 3));
        list.add(new ItemStack(id, 1, 4));
        list.add(new ItemStack(id, 1, 5));
        list.add(new ItemStack(id, 1, 6));
        list.add(new ItemStack(id, 1, 7));
        list.add(new ItemStack(id, 1, 8));
    }
    
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister) {
        this.iconArray = new Icon[machineTexture.length];

        for (int i = 0; i < this.iconArray.length; ++i)
        {
            this.iconArray[i] = par1IconRegister.registerIcon(machineTexture[i]);
        }
    }
}
