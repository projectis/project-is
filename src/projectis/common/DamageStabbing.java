package projectis.common;

import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;

public class DamageStabbing extends DamageSource {

	protected Entity damageSourceEntity;

	public DamageStabbing(String damageType, Entity par2Entity) {
		super(damageType);
		this.damageSourceEntity = par2Entity;
	}

	public String getDeathMessage(EntityLiving par1EntityLiving) {
		if (this.damageSourceEntity instanceof EntityLiving) {
			ItemStack itemstack = this.damageSourceEntity instanceof EntityLiving ? ((EntityLiving)this.damageSourceEntity).getHeldItem() : null;
			if ((itemstack != null) && itemstack.hasDisplayName()) return par1EntityLiving.getTranslatedEntityName()+ " was stabbed by " + this.damageSourceEntity.getTranslatedEntityName() + "with " + itemstack.getDisplayName();
			else return par1EntityLiving.getTranslatedEntityName()+ " was stabbed by " + this.damageSourceEntity.getTranslatedEntityName();
		}
		else return par1EntityLiving.getTranslatedEntityName()+ " guts fell out";
	}
}