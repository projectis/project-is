package projectis.common;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;

public class DamageEnergy extends DamageSource {

	protected Entity damageSourceEntity;

	public DamageEnergy(String damageType, Entity par2Entity)
	{
		super(damageType);
		this.damageSourceEntity = par2Entity;
		this.setDamageBypassesArmor();
	}

	public String getDeathMessage(EntityLiving par1EntityLiving) {

		if (this.damageSourceEntity instanceof EntityLiving) {
			ItemStack itemstack = this.damageSourceEntity instanceof EntityLiving ? ((EntityLiving)this.damageSourceEntity).getHeldItem() : null;
			if ((itemstack != null) && itemstack.hasDisplayName()) return par1EntityLiving.getTranslatedEntityName()+ " was superheated by " + this.damageSourceEntity.getTranslatedEntityName() + "with " + itemstack.getDisplayName();
			else return par1EntityLiving.getTranslatedEntityName()+ " was superheated by " + this.damageSourceEntity.getTranslatedEntityName();
		}
		else return par1EntityLiving.getTranslatedEntityName()+ " was superheated";
	}
}
