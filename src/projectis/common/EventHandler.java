package projectis.common;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.culling.Frustrum;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import net.minecraftforge.client.event.RenderLivingEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.event.ForgeSubscribe;
import org.lwjgl.opengl.GL11;

import java.util.List;

public class EventHandler {

	@ForgeSubscribe
	public void onRenderWorldLast(RenderWorldLastEvent event) {

		Minecraft mc = Minecraft.getMinecraft();
		
		EntityPlayer player = (EntityPlayer) mc.renderViewEntity;
		
		/*
		if ((player.inventory.armorItemInSlot(0) == null) || (!player.inventory.armorItemInSlot(0).hasTagCompound())) return;
		
		NBTTagCompound nbt = player.inventory.armorItemInSlot(0).getTagCompound();
		
		if ((nbt == null) || (nbt.getBoolean("bioRadar") == false)) return;
		*/

		if (!Minecraft.isGuiEnabled()) return;

		List entityList = mc.theWorld.loadedEntityList;
		Vec3 renderingVector = player.getPosition(event.partialTicks);
		Frustrum frustrum = new Frustrum();

		double viewX = player.lastTickPosX + (player.posX - player.lastTickPosX) * event.partialTicks;
		double viewY = player.lastTickPosY + (player.posY - player.lastTickPosY) * event.partialTicks;
		double viewZ = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * event.partialTicks;
		
		frustrum.setPosition(viewX, viewY, viewZ);
		Entity entity;

        for (Object anEntityList : entityList) {
            entity = (Entity) anEntityList;
            if ((entity != null) && (entity instanceof EntityLiving) && (entity.isInRangeToRenderVec3D(renderingVector)) && ((entity.ignoreFrustumCheck) || (frustrum.isBoundingBoxInFrustum(entity.boundingBox))) && entity.isEntityAlive()) {
                renderBioRadar((EntityLiving) entity, event.partialTicks, player);
            }
        }
	}

	public void renderBioRadar(EntityLiving entity, float partialTicks, Entity viewPoint) {
		
		if ((!entity.canEntityBeSeen(viewPoint)) || (entity == viewPoint)) return;

		double x = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * partialTicks;
		double y = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * partialTicks;
		double z = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * partialTicks;

		//TODO Fix this
		//int maxHealth = entity.getMaxHealth();
		//int health = entity.getHealth();
		int maxHealth = 0;
		int health = 0;
		
		//TODO Make this smoothly rotate around the entity on the y axis, will need to offset the x and z based on y

        double angle = Math.atan2(x - viewPoint.posX, z - viewPoint.posZ) - Math.PI/2;
        double xCoord = Math.sin(angle) * 1;
        double zCoord = Math.cos(angle) * 1;
		AxisAlignedBB boundingBox = entity.boundingBox;
		xCoord += (boundingBox.maxX - boundingBox.minX)/3D;
		zCoord += (boundingBox.maxZ - boundingBox.minZ)/3D;

        renderLabel(entity, "Name: " + entity.getEntityName(), (float) (x - RenderManager.renderPosX + xCoord), (float) (y - RenderManager.renderPosY + entity.height + 0.8D), (float) (z - RenderManager.renderPosZ + zCoord), 64);
        renderLabel(entity, "Health: " + health + "/" + maxHealth, (float) (x - RenderManager.renderPosX  + xCoord), (float) (y - RenderManager.renderPosY + entity.height + 0.55D), (float) (z - RenderManager.renderPosZ + zCoord), 64);
	}

	public static void renderLabel(EntityLiving par1EntityLiving, String par2Str, double par3, double par5, double par7, int par9) {
		Minecraft mc = Minecraft.getMinecraft();
		RenderManager renderManager = RenderManager.instance;

		if ((renderManager.livingPlayer == null) || (par1EntityLiving == null)) return;

		if (par1EntityLiving.getDistanceSqToEntity(renderManager.livingPlayer) <= par9 * par9) {
			FontRenderer var12 = mc.fontRenderer;
			float var13 = 1.6F;
			float var14 = 0.01666667F * var13;
			GL11.glPushMatrix();
			GL11.glTranslatef((float) par3, (float) par5, (float) par7);
			GL11.glNormal3f(0.0F, 1.0F, 0.0F);
			GL11.glRotatef(-renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
			GL11.glRotatef(renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
			GL11.glScalef(-var14, -var14, var14);
			GL11.glDisable(2896);
			GL11.glDepthMask(false);
			GL11.glDisable(2929);
			GL11.glEnable(3042);
			GL11.glBlendFunc(770, 771);
			//Tessellator var15 = Tessellator.instance;
			byte var16 = 0;
			GL11.glDisable(3553);
			/*
			var15.startDrawingQuads();
			int var17 = var12.getStringWidth(par2Str) / 2;
			var15.setColorRGBA_F(0.0F, 0.0F, 0.0F, 0.25F);
			var15.addVertex(-var17 - 1, -1 + var16, 0.0D);
			var15.addVertex(-var17 - 1, 8 + var16, 0.0D);
			var15.addVertex(var17 + 1, 8 + var16, 0.0D);
			var15.addVertex(var17 + 1, -1 + var16, 0.0D);
			var15.draw();
			*/
			GL11.glEnable(3553);
			//var12.drawString(par2Str, -var12.getStringWidth(par2Str) / 2, var16, 553648127);
			var12.drawString(par2Str, 0, var16, 553648127);
			GL11.glEnable(2929);
			GL11.glDepthMask(true);
			//var12.drawString(par2Str, -var12.getStringWidth(par2Str) / 2, var16, -1);
			var12.drawString(par2Str, 0, var16, -1);
			GL11.glEnable(2896);
			GL11.glDisable(3042);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glPopMatrix();
		}
	}
}
