package projectis.common;

import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import projectis.ProjectIS;

import java.util.Random;

public class WorldGen implements IWorldGenerator {

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		switch(world.provider.dimensionId) {
		case -1: generateNether(world,random,chunkX*16,chunkZ*16); break;
		case 0: generateSurface(world, random, chunkX *16, chunkZ * 16); break;
		case 1: generateEnd(world, random, chunkX * 16, chunkZ * 16); break;
		}
	}
		//TODO get ores to generate
	
		private void generateSurface(World world, Random rand, int chunkX, int chunkZ){
			//Gen Palladium
			for (int k = 0; k < 1; k++) {
				int i2 = chunkX + rand.nextInt(16);
				int j2 = rand.nextInt(40);
				int k2 = chunkZ + rand.nextInt(16);
				//(new WorldGenMinable(ProjectIS.orePalladium.blockID, 50)).generate(world,rand,i1,j1,k1);
				ProjectIS.palladiumGen.generate(world, rand, i2, j2, k2);
			}
			for (int m = 0; m < 2; m++) {
				int i3 = chunkX + rand.nextInt(16);
				int j3 = rand.nextInt(45);
				int k3 = chunkZ = rand.nextInt(16);
				ProjectIS.silverGen.generate(world, rand, i3, j3, k3);
			}
			//Gen Aluminium
			for (int l = 0; l < 15; l++) {
				int i1 = chunkX + rand.nextInt(16);
				int j1 = rand.nextInt(40);
				int k1 = chunkZ + rand.nextInt(16);
				ProjectIS.aluminiumGen.generate(world, rand, i1, j1, k1);
			}
			
			
			
		//world.setBlock(chunkX*16 + random.nextInt(16), 100, chunkZ*16 + random.nextInt(16), ProjectIS.oreSilverID);
		// block X location, block Y location, block Z location, block id (in this case, wooden planks)
	}
	
		private void generateEnd(World world, Random rand, int chunkX, int chunkZ) {}
		private void generateNether(World world, Random rand, int chunkX, int chunkZ){
			//Gen Selenium
			for (int j = 0; j < 2; j++) {
				int i5 = chunkX + rand.nextInt(16);
				int j5 = rand.nextInt(128);
				int k5 = chunkZ + rand.nextInt(16);
				//(new WorldGenMinable(ProjectIS.oreTungstenID.blockID, 13)).generate(world, rand, i1, j1, k1);
				ProjectIS.seleniumGen.generate(world, rand, i5, j5, k5);
			}
			//Gen Tungsten
			for (int n = 0; n < 2; n++) {
				int i4 = chunkX + rand.nextInt(16);
				int j4 = rand.nextInt(128);
				int k4 = chunkZ + rand.nextInt(16);
				//(new WorldGenMinable(ProjectIS.oreTungstenID.blockID, 13)).generate(world, rand, i1, j1, k1);
				ProjectIS.tungstenGen.generate(world, rand, i4, j4, k4);
			}
		}
}
