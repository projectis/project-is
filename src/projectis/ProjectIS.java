package projectis;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.Property;
import net.minecraftforge.oredict.OreDictionary;
import projectis.block.BlockMachines;
import projectis.block.BlockOre;
import projectis.common.EventHandler;
import projectis.common.WorldGen;
import projectis.gui.GuiHandler;
import projectis.item.*;
import projectis.proxy.*;
import projectis.tileentity.TileEntityAssembler;
import projectis.tileentity.TileEntityAssemblyPowerInlet;
import projectis.tileentity.TileEntityAssemblyProcessingUnit;

@Mod(modid = "ProjectIS", name = "Project IS", version = "0.0.1")
@NetworkMod(clientSideRequired = true)
public class ProjectIS {
	
	//Stratos
	public static Item itemExoHelmet;
	public static Item itemExoChest;
	public static Item itemExoLegs;
	public static Item itemExoFeet;
		
	//core + tier Items
	public static Item itemCore; //modular module
	public static Item itemCPUBasic; //Tier1
	public static Item itemCPUAdvanced; //Tier2
	public static Item itemCPUMulticore; //Tier3
	public static Item itemCPUMultithreaded; //Tier4
	public static Item itemCPUVPU;//Tier5
	
	//Reactors
	public static Item itemReactorFission;//Tier1
	public static Item itemReactorFusion;//Tier2
	public static Item itemReactorNuclearFusion;//Tier3
	public static Item itemReactorColdFusion;//Tier4
	public static Item itemReactorPlasma;//Tier5
	public static Item itemReactorAntimatter;//Tier5
	//Defense
	//Barriers
	public static Item itemBarrierThermic;//Tier1
	public static Item itemBarrierReactive;//Tier2
	public static Item itemBarrierElectroMagnetic;//Tier3
	public static Item itemBarrierKinetic;//Tier4
	public static Item itemBarrierReflective;//Tier5
	
	//Plating
	public static Item itemPlatingBasic;//Tier1
	public static Item itemPlatingAdvanced;//Tier2
	public static Item itemPlatingCeramic;//Tier3
	public static Item itemPlatingNano;//Tier4
	public static Item itemPlatingTungsten;//Tier5
	
	//CounterMeasures
	public static Item itemCounterFlare;//Tier1
	public static Item itemCounterAA;//Tier2
	public static Item itemCounterSAM;//Tier3
	public static Item itemCounterLaser;//Tier4
	public static Item itemCounterPlasma;//Tier4
	public static Item itemCounterAIC;//Tier5
	
	//Shields
	public static Item itemShieldBasic;//Tier1
	public static Item itemShieldAdvanced;//Tier2
	public static Item itemShieldHeavyAlloy;//Tier3
	public static Item itemShieldCyber;//Tier4
	public static Item itemShieldTungsten;//Tier5
	
	//Weapons
	//CloseRange
	public static Item itemBasicShortSword;//Tier1
	public static Item itemBasicLongSword;//Tier1
	public static Item itemHeavyAlloyShortSword;//Tier2
	public static Item itemHeavyAlloyLongSword;//Tier2
	public static Item itemCyberShortSword;//Tier3
	public static Item itemCyberLongSword;//Tier3
	public static Item itemLaserRapier;//Tier4
	public static Item itemLaserShortSword;//Tier4
	public static Item itemPlasmaLongSword;//Tier4
	public static Item itemPlasmaBardiche;//Tier4
	public static Item itemAccelFusionRapier;//Tier5
	public static Item itemAccelFusionShortSword;//Tier5
	public static Item itemAccelFusionLongSword;//Tier5
	public static Item itemAccelFusionBardiche;//Tier5
	
	//Ranged
	//In-Hand
	public static Item itemBasicPistol;//Tier1
	public static Item itemBasicMinigun;//Tier1
	public static Item itemProjectileSniper;//Tier2
	public static Item itemHeavyProjectileSMG;//Tier3
	public static Item itemHeavyProjectileMinigun;//Tier3
	public static Item itemLaserSniper;//Tier4
	public static Item itemLaserPistol;//Tier4
	public static Item itemPlasmaSMG;//Tier4
	public static Item itemPlasmaMinigun;//Tier4
	public static Item itemAccelFusionPistol;//Tier5
	public static Item itemAccelFusionSMG;//Tier5
	public static Item itemAccelFusionMinigun;//Tier5
	public static Item itemAntimatterSniper;//Tier5
	
	//ShoulderMounted
	public static Item itemProjectileRocketPod;//Tier2
	public static Item itemProjectileMinigun;//Tier2
	public static Item itemHeavyProjectileFlakCannon;//Tier3
	public static Item itemHeavyProjectileRailgun;//Tier3
	public static Item itemLaserShoulderGun;//Tier4
	public static Item itemPlasmaShoulderGun;//Tier4
	public static Item itemAccelFusionRailgun;//Tier5
	public static Item itemAccelFusionShoulderGun;//Tier5
	
	//Utilities
	//Tools
	public static Item itemUtilBasicDrill;//Tier1
	public static Item itemUtilBasicSaw;//Tier1
	public static Item itemUtilHeavyAlloyDrill;//Tier2
	public static Item itemUtilHeavyAlloySaw;//Tier2
	public static Item itemUtilLaserMiningLaser;//Tier3
	public static Item itemUtilPlasmaSaw;//Tier3
	public static Item itemUtilTungstenDrill;//Tier3
	public static Item itemUtilTungstenSaw;//Tier3
	
	//Storage
	public static Item itemStorage1;//Tier1
	public static Item itemStorage2;//Tier2
	public static Item itemQuantumStorage;//Tier3
	
	//Misc
	//PowerStorage
	public static Item itemLithiumBattery;//Tier1
	public static Item itemFuelcell;//Tier2
	public static Item itemFissionBattery;//Tier3
	public static Item itemFusionBattery;//Tier4
	public static Item itemQuantumBattery;//Tier5
	public static Item itemAntimatterContainer;//Tier5
	
	//SpeedUpgrade & HyperDrives
	public static Item itemAdditionalThrusters;//Tier1
	public static Item itemFissionThrusters;//Tier2
	public static Item itemFusionThrusters;//Tier3
	public static Item itemHyperDrivePrototype;//Tier3
	public static Item itemAccelFusionThrusters;//Tier4
	public static Item itemAntimatterHyperDrive;//Tier4
	public static Item itemOverchargedAccelFusionThrusters;//Tier5
	public static Item itemOverChargedHyperDrive;//Tier5
	
	//Detection
	public static Item itemHeatRadar;//Tier1
	public static Item itemLidar;//Tier2
	public static Item itemBioRadar;//Tier3
	
	//Biogenetics
	public static Item itemRegeneration;//Tier1
	public static Item itemBioRestoration;//Tier2
	public static Item itemBioMedicalSupport;//Tier2
	public static Item itemGeneticRebuild;//Tier3
	public static Item itemGeneticRebuildSupport;//Tier3
	
	//WaterUpgrade
	public static Item itemNauticalThrusters;//Tier1
	public static Item itemRebreather;//Tier2
	
	//Fireupgrade
	public static Item itemZonedLiquidCooling;//Tier1
	public static Item itemDivergingHeatPlating;//Tier2
	
	//Other
	public static Item itemNightVision;//Tier1
	public static Item itemCollectorMagnet;//Tier1
	
	//Item upgrades
	//Weapons
	public static Item itemUpgradeOvercharge;//post-tier5
		
	//Defense
	public static Item itemUpgradeNanites;//post-Tier3
	
	//Aspect upgrades
	public static Item itemAspectFire; //Tier1
	public static Item itemAspectEarth; //Tier1
	public static Item itemAspectIce; //Tier1
	public static Item itemAspectLightning;//Tier1
	public static Item itemAspectVoid;//Tier1
	
	public static Item itemOre;
	public static Item itemMachine;
	
	public static Block blockOre;
	public static Block blockMachine;
	
	public static int blockOreID;
	public static int blockMachineID;
	
	public static int itemExoHelmetID;
	public static int itemExoChestID;
	public static int itemExoLegsID;
	public static int itemExoFeetID;
	public static int itemCoreID;
	public static int itemCPUBasicID; 
	public static int itemCPUAdvancedID; 
	public static int itemCPUMulticoreID; 
	public static int itemCPUMultithreadedID; 
	public static int itemCPUVPUID;
	public static int itemReactorFissionID;
	public static int itemReactorFusionID;
	public static int itemReactorNuclearFusionID;
	public static int itemReactorColdFusionID;
	public static int itemReactorPlasmaID;
	public static int itemReactorAntimatterID;
	public static int itemBarrierThermicID;
	public static int itemBarrierReactiveID;
	public static int itemBarrierElectroMagneticID;
	public static int itemBarrierKineticID;
	public static int itemBarrierReflectiveID;
	public static int itemPlatingBasicID;
	public static int itemPlatingAdvancedID;
	public static int itemPlatingCeramicID;
	public static int itemPlatingNanoID;
	public static int itemPlatingTungstenID;
	public static int itemCounterFlareID;
	public static int itemCounterAAID;
	public static int itemCounterSAMID;
	public static int itemCounterLaserID;
	public static int itemCounterPlasmaID;
	public static int itemCounterAICID;
	public static int itemShieldBasicID;
	public static int itemShieldAdvancedID;
	public static int itemShieldHeavyAlloyID;
	public static int itemShieldCyberID;
	public static int itemShieldTungstenID;
	public static int itemBasicShortSwordID;
	public static int itemBasicLongSwordID;
	public static int itemHeavyAlloyShortSwordID;
	public static int itemHeavyAlloyLongSwordID;
	public static int itemCyberShortSwordID;
	public static int itemCyberLongSwordID;
	public static int itemLaserRapierID;
	public static int itemLaserShortSwordID;
	public static int itemPlasmaLongSwordID;
	public static int itemPlasmaBardicheID;
	public static int itemAccelFusionRapierID;
	public static int itemAccelFusionShortSwordID;
	public static int itemAccelFusionLongSwordID;
	public static int itemAccelFusionBardicheID;
	public static int itemBasicPistolID;
	public static int itemBasicMinigunID;
	public static int itemProjectileSniperID;
	public static int itemHeavyProjectileSMGID;
	public static int itemHeavyProjectileMinigunID;
	public static int itemLaserSniperID;
	public static int itemLaserPistolID;
	public static int itemPlasmaSMGID;
	public static int itemPlasmaMinigunID;
	public static int itemAccelFusionPistolID;
	public static int itemAccelFusionSMGID;
	public static int itemAccelFusionMinigunID;
	public static int itemAntimatterSniperID;
	public static int itemProjectileRocketPodID;
	public static int itemProjectileMinigunID;
	public static int itemHeavyProjectileFlakCannonID;
	public static int itemHeavyProjectileRailgunID;
	public static int itemLaserShoulderGunID;
	public static int itemPlasmaShoulderGunID;
	public static int itemAccelFusionRailgunID;
	public static int itemAccelFusionShoulderGunID;
	public static int itemUtilBasicDrillID;
	public static int itemUtilBasicSawID;
	public static int itemUtilHeavyAlloyDrillID;
	public static int itemUtilHeavyAlloySawID;
	public static int itemUtilLaserMiningLaserID;
	public static int itemUtilPlasmaSawID;
	public static int itemUtilTungstenDrillID;
	public static int itemUtilTungstenSawID;
	public static int itemStorage1ID;
	public static int itemStorage2ID;
	public static int itemQuantumStorageID;
	public static int itemLithiumBatteryID;
	public static int itemFuelcellID;
	public static int itemFissionBatteryID;
	public static int itemFusionBatteryID;
	public static int itemQuantumBatteryID;
	public static int itemAntimatterContainerID;
	public static int itemAdditionalThrustersID;
	public static int itemFissionThrustersID;
	public static int itemFusionThrustersID;
	public static int itemHyperDrivePrototypeID;
	public static int itemAccelFusionThrustersID;
	public static int itemAntimatterHyperDriveID;
	public static int itemOverchargedAccelFusionThrustersID;
	public static int itemOverChargedHyperDriveID;
	public static int itemHeatRadarID;
	public static int itemLidarID;
	public static int itemBioRadarID;
	public static int itemRegenerationID;
	public static int itemBioRestorationID;
	public static int itemBioMedicalSupportID;
	public static int itemGeneticRebuildID;
	public static int itemGeneticRebuildSupportID;
	public static int itemNauticalThrustersID;
	public static int itemRebreatherID;
	public static int itemZonedLiquidCoolingID;
	public static int itemDivergingHeatPlatingID;
	public static int itemNightVisionID;
	public static int itemCollectorMagnetID;
	public static int itemUpgradeOverchargeID;
	public static int itemUpgradeNanitesID;
	public static int itemAspectFireID;
	public static int itemAspectEarthID;
	public static int itemAspectIceID;
	public static int itemAspectLightningID;
	public static int itemAspectVoidID;
	
	public static WorldGenMinable silverGen;
	public static WorldGenMinable aluminiumGen;
	public static WorldGenMinable tungstenGen;
	public static WorldGenMinable palladiumGen;
	public static WorldGenMinable seleniumGen;

	@SidedProxy(clientSide = "projectis.proxy.ClientProxy", serverSide = "projectis.proxy.CommonProxy")
	public static CommonProxy proxy;
	
	@Instance("ProjectIS")
	public static ProjectIS instance;
	
	
	
	@PreInit
    public void preInit(FMLPreInitializationEvent event) {
		Configuration config = new Configuration(event.getSuggestedConfigurationFile());
		Property prop = null;
		config.load();
		
		prop = config.get(Configuration.CATEGORY_BLOCK, "Ore", 500);
		blockOreID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_BLOCK, "Machines", 501);
		blockMachineID = prop.getInt();
		
		//prop.comment = "Stratos Exo Suit";
		prop = config.get(Configuration.CATEGORY_ITEM, "Exo Helmet", 5000);
		itemExoHelmetID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Exo Chest", 5001);
		itemExoChestID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Exo Legs", 5002);
		itemExoLegsID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Exo Boots", 5003);
		itemExoFeetID = prop.getInt();
		//prop.comment = "Core + Tier System";
		prop = config.get(Configuration.CATEGORY_ITEM, "Stratos Core", 5004);
		itemCPUMultithreadedID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Basic CPU", 5005);
		itemCPUBasicID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Advanced CPU", 5006);
		itemCPUAdvancedID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Multi-core CPU", 5007);
		itemCPUMulticoreID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Multi-Threaded CPU", 5008);
		itemCPUMultithreadedID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "VPU", 5009);
		itemCPUVPUID= prop.getInt();
		//prop.comment="Power Supplies";
		prop = config.get(Configuration.CATEGORY_ITEM, "Portable Fission Reactor", 5010);
		itemReactorFissionID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Portable Fusion Reactor", 5011);
		itemReactorFusionID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Portable Nuclear Fusion Reactor", 5012);
		itemReactorNuclearFusionID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Portable Cold Fusion Reactor", 5013);
		itemReactorColdFusionID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Plasma Reactor", 5014);
		itemReactorPlasmaID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Anti-matter Reactor", 5015);
		itemReactorAntimatterID= prop.getInt();
		//prop.comment="Defensive Modules";
		//prop.comment="Barriers";
		prop = config.get(Configuration.CATEGORY_ITEM, "Thermic Screen Emitter", 5016);
		itemBarrierThermicID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Reactive Barrier Projector", 5017);
		itemBarrierReactiveID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Electro-Magnetic Field Projector", 5018);
		itemBarrierElectroMagneticID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Kinetic Force Field Emitter", 5019);
		itemBarrierKineticID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Force Vector Modifier Module", 5020);
		itemBarrierReflectiveID = prop.getInt();
		//prop.comment="Plating";
		prop = config.get(Configuration.CATEGORY_ITEM, "Steel Plating", 5021);
		itemPlatingBasicID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Palladium-Ruthenium Alloy Plating", 5022);
		itemPlatingAdvancedID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Enriched Ceramic Plating", 5023);
		itemPlatingCeramicID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Osmium-Fiber Plating", 5024);
		itemPlatingNanoID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Tungsten Carbide Plating", 5025);
		itemPlatingTungstenID = prop.getInt();
		//prop.comment="Counter Measures";
		prop = config.get(Configuration.CATEGORY_ITEM, "Flare Ejector", 5026);
		itemCounterFlareID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Anti-Air Defense Module", 5027);
		itemCounterAAID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "SAM Launcher", 5028);
		itemCounterSAMID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Laser Countermeasures", 5029);
		itemCounterLaserID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Plasma Interceptors", 5030);
		itemCounterPlasmaID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Acceleration Inertial Canceler", 5031);
		itemCounterAICID = prop.getInt();
		//prop.comment="Shields";
		prop = config.get(Configuration.CATEGORY_ITEM, "Steel Plated Shield", 5032);
		itemShieldBasicID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Palladium-Ruthenium Alloy Shield", 5033);
		itemShieldAdvancedID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Enriched Heavy Alloy Shield", 5034);
		itemShieldHeavyAlloyID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Osmium-Fiber Shield", 5035);
		itemShieldCyberID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Tungsten Carbide Shield", 5036);
		itemShieldTungstenID = prop.getInt();
		//prop.comment="Weapons";
		//prop.comment="Close-Combat";
		prop = config.get(Configuration.CATEGORY_ITEM, "Steel Sword", 5037);
		itemBasicShortSwordID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Steel Claymore", 5038);
		itemBasicLongSwordID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Heavy Alloy Sword", 5039);
		itemHeavyAlloyShortSwordID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Heavy Alloy Claymore", 5040);
		itemHeavyAlloyLongSwordID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Laser Rapier", 5041);
		itemLaserRapierID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Laser Sword", 5042);
		itemLaserShortSwordID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Plasma Claymore", 5043);
		itemPlasmaLongSwordID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Plasma Bardiche", 5044);
		itemPlasmaBardicheID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Accelerated Fusion Rapier", 5045);
		itemAccelFusionRapierID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Accelerated Fusion Sword", 5046);
		itemAccelFusionShortSwordID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Accelerated Fusion Claymore", 5047);
		itemAccelFusionLongSwordID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Accelerated Fusion Bardiche", 5048);
		itemAccelFusionBardicheID = prop.getInt();
		//prop.comment = "Ranged Combat";
		//prop.comment = "In-hand Weapons";
		prop = config.get(Configuration.CATEGORY_ITEM, "P300 Semi-Automatic Hand Gun", 5049);
		itemBasicPistolID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "M240 Machine Gun", 5050);
		itemBasicMinigunID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "APR3380 Sniper Rifle", 5051);
		itemProjectileSniperID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "CPW200 Submachine Gun", 5052);
		itemHeavyProjectileSMGID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "XM214 Gattling Gun", 5053);
		itemHeavyProjectileMinigunID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "M390 Laser Edition Sniper Rifle", 5054);
		itemLaserSniperID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Glock 400 Semi-Automatic Laser Cannon", 5055);
		itemLaserPistolID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "P9000 Plasma Submachine Gun", 5056);
		itemPlasmaSMGID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "M134 Plasma Edition Gattling Gun", 5057);
		itemPlasmaMinigunID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "VBR-PDW300 Full-Auto Fusion Hand Cannon", 5058);
		itemAccelFusionPistolID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "TDI Vector9 Fusion Submachine Gun", 5059);
		itemAccelFusionSMGID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "M796 Fusion Gattling Cannon", 5060);
		itemAccelFusionMinigunID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "AS5000 Anti-Matter Sniper Rifle", 5061);
		itemAntimatterSniperID = prop.getInt();
		//prop.comment = "Shoulder Mounted";
		prop = config.get(Configuration.CATEGORY_ITEM, "UB320 Rocket Pod", 5062);
		itemProjectileRocketPodID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "XM327 Gattling Gun", 5063);
		itemProjectileMinigunID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "GAU-80 Gatling Gun", 5064);
		itemHeavyProjectileFlakCannonID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "R7-2 Railgun", 5065);
		itemHeavyProjectileRailgunID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "LZ48 Laser Cannon", 5066);
		itemLaserShoulderGunID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "P12R Plasma Cannon", 5067);
		itemPlasmaShoulderGunID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "AF-2 Anti-matter Cannon", 5068);
		itemAccelFusionShoulderGunID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "R8-PL7 Accelerated Plasma Railgun", 5069);
		itemAccelFusionRailgunID = prop.getInt();
		//prop.comment = "Utilities";
		prop = config.get(Configuration.CATEGORY_ITEM, "Steel Drill", 5062);
		itemUtilBasicDrillID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Steel Saw", 5063);
		itemUtilBasicSawID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Heavy Alloy Drill", 5064);
		itemUtilHeavyAlloyDrillID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Heavy Alloy Saw", 5065);
		itemUtilHeavyAlloySawID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "LZR-31 Mining Laser", 5066);
		itemUtilLaserMiningLaserID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "PLM-12 Plasma Cutter", 5067);
		itemUtilPlasmaSawID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Tungsten Carbide Drill", 5068);
		itemUtilTungstenDrillID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Tungsten Carbide Saw", 5069);
		itemUtilTungstenSawID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Basic Storage", 5070);
		itemStorage1ID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Advanced Storage", 5071);
		itemStorage2ID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Quantum Storage", 5072);
		itemQuantumStorageID = prop.getInt();
		prop.comment = "Misc";
		prop.comment = "Batteries";
		prop = config.get(Configuration.CATEGORY_ITEM, "Lithium Battery", 5073);
		itemLithiumBatteryID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Fuel Cell", 5074);
		itemFuelcellID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Fission Battery", 5075);
		itemFissionBatteryID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Fusion Battery", 5076);
		itemFusionBatteryID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Quantum Battery", 5077);
		itemQuantumBatteryID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Antimatter Container", 5078);
		itemAntimatterContainerID = prop.getInt();
		prop.comment = "Speed Upgrades";
		prop = config.get(Configuration.CATEGORY_ITEM, "Thrusters", 5079);
		itemAdditionalThrustersID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Fission Thrusters", 5080);
		itemFissionThrustersID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Fusion Thrusters", 5081);
		itemFusionThrustersID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Accelerated Fusion Thrusters", 5082);
		itemAccelFusionThrustersID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Overcharged Accelerated Fusion Thrusters", 5083);
		itemOverchargedAccelFusionThrustersID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Hyperdrive Prototype", 5084);
		itemHyperDrivePrototypeID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Antimatter Hyperdrive", 5085);
		itemAntimatterHyperDriveID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Overcharged Hyperdrive", 5086);
		itemOverChargedHyperDriveID = prop.getInt();
		prop.comment = "Radars";
		prop = config.get(Configuration.CATEGORY_ITEM, "Heat Radar", 5087);
		itemHeatRadarID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Lidar", 5088);
		itemLidarID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Bio Radar", 5089);
		itemBioRadarID = prop.getInt();
		prop.comment = "Biogenetics";
		prop = config.get(Configuration.CATEGORY_ITEM, "Regeneration", 5090);
		itemRegenerationID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Bio Restoration", 5091);
		itemBioRestorationID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Bio Medical Support", 5092);
		itemBioMedicalSupportID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Genetic Rebuild Module", 5093);
		itemGeneticRebuildID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Genetic Rebuild Support Module", 5094);
		itemGeneticRebuildSupportID = prop.getInt();
		prop.comment = "Extra";
		prop = config.get(Configuration.CATEGORY_ITEM, "Nautical Thrusters", 5095);
		itemNauticalThrustersID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Rebreather", 5096);
		itemRebreatherID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Zoned Liquid Cooling", 5097);
		itemZonedLiquidCoolingID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Diverging Heat Plating", 5098);
		itemDivergingHeatPlatingID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Night Vision Module", 5099);
		itemNightVisionID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Collector Magnet", 5100);
		itemCollectorMagnetID = prop.getInt();
		prop.comment="Upgrades";
		prop = config.get(Configuration.CATEGORY_ITEM, "Upgrade: Overcharge", 5101);
		itemUpgradeOverchargeID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Upgrade: Nanites", 5102);
		itemUpgradeNanitesID = prop.getInt();
		prop.comment="Aspects";
		prop = config.get(Configuration.CATEGORY_ITEM, "Aspect: Fire", 5103);
		itemAspectFireID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Aspect: Earth", 5104);
		itemAspectEarthID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Aspect: Ice", 5105);
		itemAspectIceID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Aspect: Lightning", 5106);
		itemAspectLightningID = prop.getInt();
		prop = config.get(Configuration.CATEGORY_ITEM, "Aspect: Void", 5107);
		itemAspectVoidID = prop.getInt();
		config.save();
		
	}
	
	@Init
	public void load(FMLInitializationEvent event) {
		
		//Register Blocks		
		blockOre = new BlockOre(blockOreID, Material.ground);
		GameRegistry.registerBlock(blockOre, ItemOre.class, "blockOre");
		for (int i = 0; i < 5; i++) {
			ItemStack multiBlockStack = new ItemStack(blockOre, 1, i);
			LanguageRegistry.addName(multiBlockStack, BlockOre.oreType[multiBlockStack.getItemDamage()]);
		}
		
		blockMachine = new BlockMachines(blockMachineID, Material.iron);
		GameRegistry.registerBlock(blockMachine, ItemMachines.class,"blockMachine");
		for (int i = 0; i < 9; i++) {
			ItemStack multiBlockStack2 = new ItemStack(blockMachine, 1, i);
			LanguageRegistry.addName(multiBlockStack2, BlockMachines.machineType[multiBlockStack2.getItemDamage()]);
		}
		
		//Register Tile Entities
		GameRegistry.registerTileEntity(TileEntityAssemblyProcessingUnit.class, "assemblyPU");
		GameRegistry.registerTileEntity(TileEntityAssembler.class, "assembler");
		GameRegistry.registerTileEntity(TileEntityAssemblyPowerInlet.class, "assemblyPI");
		
		//Register Items
		this.itemExoHelmet = new ItemExoHelmet(this.itemExoHelmetID, EnumArmorMaterial.DIAMOND, 3, 0);
		LanguageRegistry.addName(this.itemExoHelmet, "Exo Helmet");
		GameRegistry.registerItem(this.itemExoHelmet, "itemExoHelmet");
		
		this.itemExoChest = new ItemExoChest(this.itemExoChestID, EnumArmorMaterial.DIAMOND, 3, 1);
		LanguageRegistry.addName(this.itemExoChest, "Exo Chest");
		GameRegistry.registerItem(this.itemExoChest, "itemExoChest");
		
		this.itemExoLegs = new ItemExoLegs(this.itemExoLegsID, EnumArmorMaterial.DIAMOND, 3, 2);
		LanguageRegistry.addName(this.itemExoLegs, "Exo Legs");
		GameRegistry.registerItem(this.itemExoLegs, "itemExoLegs");
		
		this.itemExoFeet = new ItemExoFeet(this.itemExoFeetID, EnumArmorMaterial.DIAMOND, 3, 3);
		LanguageRegistry.addName(this.itemExoFeet, "Exo Feet");
		GameRegistry.registerItem(this.itemExoFeet, "itemExoFeet");
		
		this.itemCore = new ItemCore(this.itemCoreID);
		LanguageRegistry.addName(this.itemCore, "Modular Core");
		this.itemCPUBasic = new ItemCPUBasic(this.itemCPUBasicID);
		LanguageRegistry.addName(this.itemCPUBasic, "Basic CPU");
		this.itemCPUAdvanced = new ItemCPUAdvanced(this.itemCPUAdvancedID);
		LanguageRegistry.addName(this.itemCPUAdvanced, "Advanced CPU");
		this.itemCPUMulticore = new ItemCPUMulticore(this.itemCPUMulticoreID);
		LanguageRegistry.addName(this.itemCPUMulticore, "Multi-core CPU");
		this.itemCPUMultithreaded = new ItemCPUMultithreaded(this.itemCPUMultithreadedID);
		LanguageRegistry.addName(this.itemCPUMultithreaded, "Multi-Threaded CPU");
		this.itemCPUVPU = new ItemCPUVPU(this.itemCPUVPUID);
		LanguageRegistry.addName(this.itemCPUVPU, "Vector Processing Unit");
		this.itemReactorFission = new ItemReactorFission(this.itemReactorFissionID);
		LanguageRegistry.addName(this.itemReactorFission, "Portable Fission Reactor");
		this.itemReactorFusion = new ItemReactorFusion(this.itemReactorFusionID);
		LanguageRegistry.addName(this.itemReactorFusion, "Portable Fusion Reactor");
		this.itemReactorNuclearFusion = new ItemReactorNuclearFusion(this.itemReactorNuclearFusionID);
		LanguageRegistry.addName(this.itemReactorNuclearFusion, "Portable Nuclear Fusion Reactor");
		this.itemReactorColdFusion = new ItemReactorColdFusion(this.itemReactorColdFusionID);
		LanguageRegistry.addName(this.itemReactorColdFusion, "Portable Cold Fusion Reactor");
		this.itemReactorPlasma = new ItemReactorPlasma(this.itemReactorPlasmaID);
		LanguageRegistry.addName(this.itemReactorPlasma, "Portable Plasma Reactor");
		this.itemReactorAntimatter = new ItemReactorAntimatter(this.itemReactorAntimatterID);
		LanguageRegistry.addName(this.itemReactorAntimatter, "Portable Anti-matter Reactor");
		this.itemBarrierThermic = new ItemBarrierThermic(this.itemBarrierThermicID);
		LanguageRegistry.addName(this.itemBarrierThermic, "Thermic Screen Emitter");
		this.itemBarrierReactive = new ItemBarrierReactive(this.itemBarrierReactiveID);
		LanguageRegistry.addName(this.itemBarrierReactive, "Reactive Barrier Projector");
		this.itemBarrierElectroMagnetic = new ItemBarrierElectroMagnetic(this.itemBarrierElectroMagneticID);
		LanguageRegistry.addName(this.itemBarrierElectroMagnetic, "Electro-Magnetic Field Projector");
		this.itemBarrierKinetic = new ItemBarrierKinetic(this.itemBarrierKineticID);
		LanguageRegistry.addName(this.itemBarrierKinetic, "Kinetic Force Field Emitter");
		this.itemBarrierReflective = new ItemBarrierReflective(this.itemBarrierReflectiveID);
		LanguageRegistry.addName(this.itemBarrierReflective, "Force Vector Modifier Module");
		this.itemPlatingBasic = new ItemPlatingBasic(this.itemPlatingBasicID);
		LanguageRegistry.addName(this.itemPlatingBasic, "Steel Plating");
		this.itemPlatingAdvanced = new ItemPlatingAdvanced(this.itemPlatingAdvancedID);
		LanguageRegistry.addName(this.itemPlatingAdvanced, "Palladium-Ruthenium Alloy Plating");
		this.itemPlatingCeramic = new ItemPlatingCeramic(this.itemPlatingCeramicID);
		LanguageRegistry.addName(this.itemPlatingCeramic, "Enriched Ceramic Plating");
		this.itemPlatingNano = new ItemPlatingNano(this.itemPlatingNanoID);
		LanguageRegistry.addName(this.itemPlatingNano, "Osmium-Fiber Plating");
		this.itemPlatingTungsten = new ItemPlatingTungsten(this.itemPlatingTungstenID);
		LanguageRegistry.addName(this.itemPlatingTungsten, "Tungsten Carbide Plating");
		this.itemCounterFlare = new ItemCounterFlare(this.itemCounterFlareID);
		LanguageRegistry.addName(this.itemCounterFlare, "Flare Ejector");
		this.itemCounterAA = new ItemCounterAA(this.itemCounterAAID);
		LanguageRegistry.addName(this.itemCounterAA, "Anti-Air Defense Module");
		this.itemCounterSAM = new ItemCounterSAM(this.itemCounterSAMID);
		LanguageRegistry.addName(this.itemCounterSAM, "SAM Launcher");
		this.itemCounterLaser = new ItemCounterLaser(this.itemCounterLaserID);
		LanguageRegistry.addName(this.itemCounterLaser, "Laser Countermeasures");
		this.itemCounterPlasma = new ItemCounterPlasma(this.itemCounterPlasmaID);
		LanguageRegistry.addName(this.itemCounterPlasma, "Plasma Interceptors");
		this.itemCounterAIC= new ItemCounterAIC(this.itemCounterAICID);
		LanguageRegistry.addName(this.itemCounterAIC, "Acceleration Inertial Canceler");
		this.itemShieldBasic = new ItemProtectionBasicShield(this.itemShieldBasicID);
		LanguageRegistry.addName(this.itemShieldBasic, "Steel Plated Shield");
		this.itemShieldAdvanced = new ItemProtectionAdvancedShield(this.itemShieldAdvancedID);
		LanguageRegistry.addName(this.itemShieldAdvanced, "Palladium-Ruthenium Alloy Shield");
		this.itemShieldHeavyAlloy = new ItemProtectionHeavyAlloyShield(this.itemShieldHeavyAlloyID);
		LanguageRegistry.addName(this.itemShieldHeavyAlloy, "Enriched Heavy Alloy Shield");
		this.itemShieldCyber = new ItemProtectionCyberShield(this.itemShieldCyberID);
		LanguageRegistry.addName(this.itemShieldCyber, "Osmium-Fiber Shield");
		this.itemShieldTungsten= new ItemProtectionTungstenShield(this.itemShieldTungstenID);
		LanguageRegistry.addName(this.itemShieldTungsten, "Tungsten Carbide Shield");
		this.itemBasicShortSword= new ItemWeaponBasicShortSword(this.itemBasicShortSwordID);
		LanguageRegistry.addName(this.itemBasicShortSword, "Steel Sword");
		this.itemBasicLongSword = new ItemWeaponBasicLongSword(this.itemBasicLongSwordID);
		LanguageRegistry.addName(this.itemBasicLongSword, "Steel Claymore");
		this.itemHeavyAlloyShortSword = new ItemWeaponHeavyAlloyShortSword(this.itemHeavyAlloyShortSwordID);
		LanguageRegistry.addName(this.itemHeavyAlloyShortSword, "Heavy Alloy Sword");
		this.itemHeavyAlloyLongSword = new ItemWeaponHeavyAlloyLongSword(this.itemHeavyAlloyLongSwordID);
		LanguageRegistry.addName(this.itemHeavyAlloyLongSword, "Heavy Alloy Claymore");
		this.itemCyberShortSword = new ItemWeaponCyberShortSword(this.itemCyberShortSwordID);
		LanguageRegistry.addName(this.itemCyberShortSword, "Cyber Sword");
		this.itemCyberLongSword= new ItemWeaponCyberLongSword(this.itemCyberLongSwordID);
		LanguageRegistry.addName(this.itemCyberLongSword, "Cyber Claymore");
		this.itemLaserRapier= new ItemWeaponLaserRapier(this.itemLaserRapierID);
		LanguageRegistry.addName(this.itemLaserRapier, "Laser Rapier");
		this.itemLaserShortSword = new ItemWeaponLaserShortSword(this.itemLaserShortSwordID);
		LanguageRegistry.addName(this.itemLaserShortSword, "Laser Sword");
		this.itemPlasmaLongSword = new ItemWeaponPlasmaLongSword(this.itemPlasmaLongSwordID);
		LanguageRegistry.addName(this.itemPlasmaLongSword, "Plasma Claymore");
		this.itemPlasmaBardiche = new ItemWeaponPlasmaBardiche(this.itemPlasmaBardicheID);
		LanguageRegistry.addName(this.itemPlasmaBardiche, "Plasma Bardiche");
		this.itemAccelFusionRapier = new ItemWeaponAccelFusionRapier(this.itemAccelFusionRapierID);
		LanguageRegistry.addName(this.itemAccelFusionRapier, "Accelerated Fusion Rapier");
		this.itemAccelFusionShortSword= new ItemWeaponAccelFusionShortSword(this.itemAccelFusionShortSwordID);
		LanguageRegistry.addName(this.itemAccelFusionShortSword, "Accelerated Fusion Sword");
		this.itemAccelFusionLongSword = new ItemWeaponAccelFusionLongSword(this.itemAccelFusionLongSwordID);
		LanguageRegistry.addName(this.itemAccelFusionLongSword, "Accelerated Fusion Claymore");
		this.itemAccelFusionBardiche = new ItemWeaponAccelFusionBardiche(this.itemAccelFusionBardicheID);
		LanguageRegistry.addName(this.itemAccelFusionBardiche, "Accelerated Fusion Bardiche");
		this.itemBasicPistol = new ItemWeaponBasicPistol(this.itemBasicPistolID);
		LanguageRegistry.addName(this.itemBasicPistol, "P300 Semi-Automatic Hand Gun");
		this.itemBasicMinigun = new ItemWeaponBasicMinigun(this.itemBasicMinigunID);
		LanguageRegistry.addName(this.itemBasicMinigun, "M240 Machine Gun");
		this.itemProjectileSniper = new ItemWeaponProjectileSniper(this.itemProjectileSniperID);
		LanguageRegistry.addName(this.itemProjectileSniper, "APR3380 Sniper Rifle");
		this.itemHeavyProjectileSMG= new ItemWeaponHeavyProjectileSMG(this.itemHeavyProjectileSMGID);
		LanguageRegistry.addName(this.itemHeavyProjectileSMG, "CPW200 Submachine Gun");
		this.itemHeavyProjectileMinigun= new ItemWeaponHeavyProjectileMinigun(this.itemHeavyProjectileMinigunID);
		LanguageRegistry.addName(this.itemHeavyProjectileMinigun, "XM214 Gattling Gun");
		this.itemLaserSniper = new ItemWeaponLaserSniper(this.itemLaserSniperID);
		LanguageRegistry.addName(this.itemLaserSniper, "M390 Laser Edition Sniper Rifle");
		this.itemLaserPistol = new ItemWeaponLaserPistol(this.itemLaserPistolID);
		LanguageRegistry.addName(this.itemLaserPistol, "Glock 400 Semi-Automatic Laser Cannon");
		this.itemPlasmaSMG = new ItemWeaponPlasmaSMG(this.itemPlasmaSMGID);
		LanguageRegistry.addName(this.itemPlasmaSMG, "P9000 Plasma Submachine Gun");
		this.itemPlasmaMinigun = new ItemWeaponPlasmaMinigun(this.itemPlasmaMinigunID);
		LanguageRegistry.addName(this.itemPlasmaMinigun, "M134 Plasma Edition Gattling Gun");
		this.itemAccelFusionPistol= new ItemWeaponAccelFusionPistol(this.itemAccelFusionPistolID);
		LanguageRegistry.addName(this.itemAccelFusionPistol, "VBR-PDW300 Full-Auto Fusion Hand Cannon");
		this.itemAccelFusionSMG = new ItemWeaponAccelFusionSMG(this.itemAccelFusionSMGID);
		LanguageRegistry.addName(this.itemAccelFusionSMG, "TDI Vector9 Fusion Submachine Gun");
		this.itemAccelFusionMinigun = new ItemWeaponAccelFusionMinigun(this.itemAccelFusionMinigunID);
		LanguageRegistry.addName(this.itemAccelFusionMinigun, "M796 Fusion Gattling Cannon");
		this.itemAntimatterSniper = new ItemWeaponAntimatterSniper(this.itemAntimatterSniperID);
		LanguageRegistry.addName(this.itemAntimatterSniper, "AS5000 Anti-Matter Sniper Rifle");
		this.itemProjectileRocketPod = new ItemWeaponProjectileRocketPod(this.itemProjectileRocketPodID);
		LanguageRegistry.addName(this.itemProjectileRocketPod, "UB320 Rocket Pod");
		this.itemProjectileMinigun = new ItemWeaponProjectileMinigun(this.itemProjectileMinigunID);
		LanguageRegistry.addName(this.itemProjectileMinigun, "XM327 Gattling Gun");
		this.itemHeavyProjectileFlakCannon = new ItemWeaponHeavyProjectileFlakCannon(this.itemHeavyProjectileFlakCannonID);
		LanguageRegistry.addName(this.itemHeavyProjectileFlakCannon, "GAU-80 Gatling Gun");
		this.itemHeavyProjectileRailgun = new ItemWeaponHeavyProjectileRailgun(this.itemHeavyProjectileRailgunID);
		LanguageRegistry.addName(this.itemHeavyProjectileRailgun, "R7-2 Railgun");
		this.itemLaserShoulderGun= new ItemWeaponLaserShoulderGun(this.itemLaserShoulderGunID);
		LanguageRegistry.addName(this.itemLaserShoulderGun, "LZ48 Laser Cannon");
		this.itemPlasmaShoulderGun = new ItemWeaponPlasmaShoulderGun(this.itemPlasmaShoulderGunID);
		LanguageRegistry.addName(this.itemPlasmaShoulderGun, "P12R Plasma Cannon");
		this.itemAccelFusionRailgun = new ItemWeaponAccelFusionRailgun(this.itemAccelFusionRailgunID);
		LanguageRegistry.addName(this.itemAccelFusionRailgun, "R8-PL7 Accelerated Plasma Railgun");
		this.itemAccelFusionShoulderGun = new ItemWeaponAccelFusionShoulderGun(this.itemAccelFusionShoulderGunID);
		LanguageRegistry.addName(this.itemAccelFusionShoulderGun, "AF-2 Anti-matter Cannon");
		this.itemUtilBasicDrill = new ItemUtilBasicDrill(this.itemUtilBasicDrillID);
		LanguageRegistry.addName(this.itemUtilBasicDrill, "Steel Drill");
		this.itemUtilBasicSaw = new ItemUtilBasicSaw(this.itemUtilBasicSawID);
		LanguageRegistry.addName(this.itemUtilBasicSaw, "Steel Saw");
		this.itemUtilHeavyAlloyDrill = new ItemUtilHeavyAlloyDrill(this.itemUtilHeavyAlloyDrillID);
		LanguageRegistry.addName(this.itemUtilHeavyAlloyDrill, "Heavy Alloy Drill");
		this.itemUtilHeavyAlloySaw = new ItemUtilHeavyAlloySaw(this.itemUtilHeavyAlloySawID);
		LanguageRegistry.addName(this.itemUtilHeavyAlloySaw, "Heavy Alloy Saw");
		this.itemUtilLaserMiningLaser= new ItemUtilLaserMiningLaser(this.itemUtilLaserMiningLaserID);
		LanguageRegistry.addName(this.itemUtilLaserMiningLaser, "LZR-31 Mining Laser");
		this.itemUtilPlasmaSaw = new ItemUtilPlasmaSaw(this.itemUtilPlasmaSawID);
		LanguageRegistry.addName(this.itemUtilPlasmaSaw, "PLM-12 Plasma Cutter");
		this.itemUtilTungstenDrill = new ItemUtilTungstenDrill(this.itemUtilTungstenDrillID);
		LanguageRegistry.addName(this.itemUtilTungstenDrill, "Tungsten Carbide Drill");
		this.itemUtilTungstenSaw = new ItemUtilTungstenSaw(this.itemUtilTungstenSawID);
		LanguageRegistry.addName(this.itemUtilTungstenSaw, "Tungsten Carbide Saw");
		this.itemStorage1 = new ItemUtilStorage1(this.itemStorage1ID);
		LanguageRegistry.addName(this.itemStorage1, "Basic Storage");
		this.itemStorage2 = new ItemUtilStorage2(this.itemStorage2ID);
		LanguageRegistry.addName(this.itemStorage2, "Advanced Storage");
		this.itemQuantumStorage = new ItemUtilQuantumStorage(this.itemQuantumStorageID);
		LanguageRegistry.addName(this.itemQuantumStorage, "Quantum Storage");
		this.itemLithiumBattery = new ItemMiscLithiumBattery(this.itemLithiumBatteryID);
		LanguageRegistry.addName(this.itemLithiumBattery, "Lithium Battery");
		this.itemFuelcell = new ItemMiscFuelCell(this.itemFuelcellID);
		LanguageRegistry.addName(this.itemFuelcell, "Fuel Cell");
		this.itemFissionBattery = new ItemMiscFissionBattery(this.itemFissionBatteryID);
		LanguageRegistry.addName(this.itemFissionBattery, "Fission Battery");
		this.itemFusionBattery = new ItemMiscFusionBattery(this.itemFusionBatteryID);
		LanguageRegistry.addName(this.itemFusionBattery, "Fusion Battery");
		this.itemQuantumBattery = new ItemMiscQuantumBattery(this.itemQuantumBatteryID);
		LanguageRegistry.addName(this.itemQuantumBattery, "Quantum Battery");
		this.itemAntimatterContainer = new ItemMiscAntimatterContainer(this.itemAntimatterContainerID);
		LanguageRegistry.addName(this.itemAntimatterContainer, "Antimatter Container");
		this.itemAdditionalThrusters = new ItemMiscAdditionalThrusters(this.itemAdditionalThrustersID);
		LanguageRegistry.addName(this.itemAdditionalThrusters, "Additional Thrusters");
		this.itemFissionThrusters = new ItemMiscFissionThrusters(this.itemFissionThrustersID);
		LanguageRegistry.addName(this.itemFissionThrusters, "Fission Thrusters");
		this.itemFusionThrusters = new ItemMiscFusionThrusters(this.itemFusionThrustersID);
		LanguageRegistry.addName(this.itemFusionThrusters, "Fusion Thrusters");
		this.itemAccelFusionThrusters = new ItemMiscAccelFusionThrusters(this.itemAccelFusionThrustersID);
		LanguageRegistry.addName(this.itemAccelFusionThrusters, "Accelerated Fusion Thrusters");
		this.itemOverchargedAccelFusionThrusters = new ItemMiscOverchargedAccelFusionThrusters(this.itemOverchargedAccelFusionThrustersID);
		LanguageRegistry.addName(this.itemOverchargedAccelFusionThrusters, "Overcharged Accelerated Fusion Thrusters");
		this.itemHyperDrivePrototype = new ItemMiscHyperdrivePrototype(this.itemHyperDrivePrototypeID);
		LanguageRegistry.addName(this.itemHyperDrivePrototype, "Hyperdrive Prototype");
		this.itemAntimatterHyperDrive = new ItemMiscAntimatterHyperdrive(this.itemAntimatterHyperDriveID);
		LanguageRegistry.addName(this.itemAntimatterHyperDrive, "Anti-matter Hyperdrive");
		this.itemOverChargedHyperDrive = new ItemMiscOverchargedHyperdrive(this.itemOverChargedHyperDriveID);
		LanguageRegistry.addName(this.itemOverChargedHyperDrive, "Overcharged Hyperdrive");
		this.itemHeatRadar = new ItemMiscHeatRadar(this.itemHeatRadarID);
		LanguageRegistry.addName(this.itemHeatRadar, "Heat Radar");
		this.itemLidar = new ItemMiscLidar(this.itemLidarID);
		LanguageRegistry.addName(this.itemLidar, "LIDAR");
		this.itemBioRadar = new ItemMiscBioRadar(this.itemBioRadarID);
		LanguageRegistry.addName(this.itemBioRadar, "Bio Radar");
		this.itemRegeneration = new ItemMiscRegeneration(this.itemRegenerationID);
		LanguageRegistry.addName(this.itemRegeneration, "Regeneration Module");
		this.itemBioRestoration = new ItemMiscBioRestoration(this.itemBioRestorationID);
		LanguageRegistry.addName(this.itemBioRestoration, "Bio Restoration");
		this.itemBioMedicalSupport = new ItemMiscBioMedSupport(this.itemBioMedicalSupportID);
		LanguageRegistry.addName(this.itemBioRadar, "Bio-Medical Support Unit");
		this.itemGeneticRebuild = new ItemMiscGeneticRebuild(this.itemGeneticRebuildID);
		LanguageRegistry.addName(this.itemGeneticRebuild, "Genetic Rebuild Module");
		this.itemGeneticRebuildSupport = new ItemMiscGeneticRebuildSupport(this.itemGeneticRebuildSupportID);
		LanguageRegistry.addName(this.itemGeneticRebuildSupport, "Genetic Rebuild Support Module");
		this.itemNauticalThrusters = new ItemMiscNauticalThrusters(this.itemNauticalThrustersID);
		LanguageRegistry.addName(this.itemNauticalThrusters, "Nautical Thrusters");
		this.itemRebreather = new ItemMiscRebreather(this.itemRebreatherID);
		LanguageRegistry.addName(this.itemRebreather, "Rebreather Add-on");
		this.itemZonedLiquidCooling = new ItemMiscZonedLiquidCooling(this.itemZonedLiquidCoolingID);
		LanguageRegistry.addName(this.itemZonedLiquidCooling, "Zoned Liquid Cooling");
		this.itemDivergingHeatPlating = new ItemMiscDivergingHeatPlating(this.itemDivergingHeatPlatingID);
		LanguageRegistry.addName(this.itemDivergingHeatPlating, "Diverging Heat Plating");
		this.itemNightVision = new ItemMiscNightVision(this.itemNightVisionID);
		LanguageRegistry.addName(this.itemNightVision, "Night Vision Module");
		this.itemCollectorMagnet = new ItemMiscCollectorMagnet(this.itemCollectorMagnetID);
		LanguageRegistry.addName(this.itemCollectorMagnet, "Collector Magnet");
		this.itemUpgradeOvercharge = new ItemUpgradeWeaponOvercharge(this.itemUpgradeOverchargeID);
		LanguageRegistry.addName(this.itemUpgradeOvercharge, "Upgrade: Overcharge");
		this.itemUpgradeNanites = new ItemUpgradeBarrierNanite(this.itemUpgradeNanitesID);
		LanguageRegistry.addName(this.itemUpgradeNanites, "Upgrade: Nanites");
		this.itemAspectFire = new ItemAspectFire(this.itemAspectFireID);
		LanguageRegistry.addName(this.itemAspectFire, "Aspect: Fire");
		this.itemAspectEarth = new ItemAspectEarth(this.itemAspectEarthID);
		LanguageRegistry.addName(this.itemAspectEarth, "Aspect: Earth");
		this.itemAspectIce = new ItemAspectIce(this.itemAspectIceID);
		LanguageRegistry.addName(this.itemAspectIce, "Aspect: Ice");
		this.itemAspectLightning = new ItemAspectLightning(this.itemAspectLightningID);
		LanguageRegistry.addName(this.itemAspectLightning, "Aspect: Lightning");
		this.itemAspectVoid = new ItemAspectVoid(this.itemAspectVoidID);
		LanguageRegistry.addName(this.itemAspectVoid, "Aspect: Void");
		
		//Register Ores in Ore Dictionary
		OreDictionary.registerOre("orePalladium", new ItemStack(blockOre, 1, 1));
		OreDictionary.registerOre("oreSilver", new ItemStack(blockOre, 1, 2));
		OreDictionary.registerOre("oreTungsten", new ItemStack(blockOre, 1, 3));
		OreDictionary.registerOre("oreSelenium", new ItemStack(blockOre, 1, 4));
		OreDictionary.registerOre("oreAluminum", new ItemStack(blockOre, 1, 0));
		
		//World Gen
		this.silverGen = new WorldGenMinable(this.blockOre.blockID, 2, 8, Block.stone.blockID);
		this.aluminiumGen = new WorldGenMinable(this.blockOre.blockID, 0, 8, Block.stone.blockID);
		this.tungstenGen = new WorldGenMinable(this.blockOre.blockID, 3, 2, Block.stone.blockID);
		this.palladiumGen = new WorldGenMinable(this.blockOre.blockID, 1, 2, Block.stone.blockID);
		this.seleniumGen = new WorldGenMinable(this.blockOre.blockID, 4, 2, Block.stone.blockID);
		GameRegistry.registerWorldGenerator(new WorldGen());
		
		NetworkRegistry.instance().registerGuiHandler(this, new GuiHandler());
		
		MinecraftForge.EVENT_BUS.register(new EventHandler());
	}
}
